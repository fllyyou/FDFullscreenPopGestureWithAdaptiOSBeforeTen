//
//  TNBaseViewController.m
//  customVCHidden
//
//  Created by RENREN on 2018/1/3.
//  Copyright © 2018年 RENREN. All rights reserved.
//

#import "TNBaseViewController.h"

@interface TNBaseViewController ()

@end

@implementation TNBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

+(NSString *)getStoryBoardName
{
    return NSStringFromClass([self class]);
}

@end
