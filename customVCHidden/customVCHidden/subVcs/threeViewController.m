//
//  threeViewController.m
//  testNaviHidden
//
//  Created by RENREN on 2018/1/2.
//  Copyright © 2018年 RENREN. All rights reserved.
//

#import "threeViewController.h"
#import "fourViewController.h"

@interface threeViewController ()

@end

@implementation threeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)fourNext:(id)sender {
    [self.navigationController pushViewController:[fourViewController new] animated:YES];
}

-(BOOL)getNaviHidden
{
    return YES;
}

@end
