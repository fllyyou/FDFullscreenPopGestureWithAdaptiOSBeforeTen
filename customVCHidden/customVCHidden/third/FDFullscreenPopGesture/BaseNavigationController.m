//
//  BaseNavigationController.m
//  customVCHidden
//
//  Created by RENREN on 2018/1/3.
//  Copyright © 2018年 RENREN. All rights reserved.
//

#import "BaseNavigationController.h"
#import "BaseViewController.h"

@interface BaseNavigationController ()

@end

@implementation BaseNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)navigationController:(UINavigationController *)navigationController willShowViewController:(BaseViewController *)viewController animated:(BOOL)animated
{
    if ([UIDevice currentDevice].systemVersion.floatValue < 10.0 && [viewController isKindOfClass:[BaseViewController class]]) {
        if (!viewController.pushed) {
            [self.navigationBar setHidden:[viewController getNaviHidden]];
            viewController.pushed = YES;
        }else
        {
            [self setNavigationBarHidden:[viewController getNaviHidden] animated:animated];
        }
    }
}


@end
