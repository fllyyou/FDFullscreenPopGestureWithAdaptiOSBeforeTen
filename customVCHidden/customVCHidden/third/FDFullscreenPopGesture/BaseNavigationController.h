//
//  BaseNavigationController.h
//  customVCHidden
//
//  Created by RENREN on 2018/1/3.
//  Copyright © 2018年 RENREN. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseNavigationController : UINavigationController<UINavigationControllerDelegate>

/**
 如果子类实现了 UINavigationControllerDelegate的方法，需要进行super 调用

 */
-(void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated;

@end
