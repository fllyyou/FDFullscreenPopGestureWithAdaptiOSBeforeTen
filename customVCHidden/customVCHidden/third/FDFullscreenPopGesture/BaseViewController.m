//
//  BaseViewController.m
//  customVCHidden
//
//  Created by RENREN on 2018/1/3.
//  Copyright © 2018年 RENREN. All rights reserved.
//

#import "BaseViewController.h"
#import "UINavigationController+FDFullscreenPopGesture.h"

@interface BaseViewController ()

@property (nonatomic) BOOL isCancelTran;

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if ([UIDevice currentDevice].systemVersion.floatValue >= 10.0) {
        self.fd_prefersNavigationBarHidden = [self getNaviHidden];
    }
    self.fd_interactivePopDisabled = [self getPanBackAction];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/**
 是否隐藏导航栏 default no （不隐藏）
 
 @return yes or no
 */
-(BOOL)getNaviHidden
{
    return self.rr_navHidden;
}
/**
 是否取消返回当前页面的返回手势 default no（不取消返回手势）
 
 @return yes or no
 */
-(BOOL)getPanBackAction
{
    return self.rr_backActionDisAble;
}
//对ios 10以下的导航栏进行兼容处理，防止导航栏错乱
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //ios对转场动画进行监听
    if ([UIDevice currentDevice].systemVersion.floatValue < 10.0) {
        id<UIViewControllerTransitionCoordinator> tc = self.transitionCoordinator;
        if (tc && [tc initiallyInteractive]) {
            [tc notifyWhenInteractionEndsUsingBlock:^(id<UIViewControllerTransitionCoordinatorContext>  _Nonnull context) {
                BaseViewController *fromVc = [context viewControllerForKey:UITransitionContextFromViewControllerKey];
                if (context.isCancelled) {
                    fromVc.isCancelTran = YES;
                }
            }];
            [tc animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext>  _Nonnull context) {
                BaseViewController *toVc = [context viewControllerForKey:UITransitionContextToViewControllerKey];
                BaseViewController *fromVc = [context viewControllerForKey:UITransitionContextFromViewControllerKey];
                if (([toVc getNaviHidden] == [fromVc getNaviHidden])&&[toVc getNaviHidden]) {
                    toVc.navigationController.navigationBar.hidden = YES;
                }else
                {
                    [toVc.navigationController setNavigationBarHidden:[toVc getNaviHidden] animated:YES];
                }
            } completion:^(id<UIViewControllerTransitionCoordinatorContext>  _Nonnull context) {
                BaseViewController *toVc = [context viewControllerForKey:UITransitionContextToViewControllerKey];
                BaseViewController *fromVc = [context viewControllerForKey:UITransitionContextFromViewControllerKey];
                BaseViewController *target = self.isCancelTran ? fromVc : toVc;
                [target.navigationController setNavigationBarHidden:[target getNaviHidden] animated:NO];
                self.isCancelTran = NO;
            }];
        }else
        {
            [self.navigationController setNavigationBarHidden:[self getNaviHidden] animated:animated];
        }
    }
}

-(void)setRr_navHidden:(BOOL)rr_navHidden
{
    _rr_navHidden = rr_navHidden;
    if ([UIDevice currentDevice].systemVersion.floatValue >= 10.0) {
        self.fd_prefersNavigationBarHidden = [self getNaviHidden];
    }
}

-(void)setRr_backActionDisAble:(BOOL)rr_backActionDisAble
{
    _rr_backActionDisAble = rr_backActionDisAble;
    self.fd_interactivePopDisabled = [self getPanBackAction];
}

@end
