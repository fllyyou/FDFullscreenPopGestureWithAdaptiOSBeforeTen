//
//  ViewController.m
//  customVCHidden
//
//  Created by RENREN on 2018/1/3.
//  Copyright © 2018年 RENREN. All rights reserved.
//

#import "ViewController.h"
#import "secViewController.h"
#import "fourViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.title = @"one";
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)next:(id)sender {
    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:[secViewController getStoryBoardName]];
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)fourNext:(id)sender {
    [self.navigationController pushViewController:[fourViewController new] animated:YES];
}


@end
