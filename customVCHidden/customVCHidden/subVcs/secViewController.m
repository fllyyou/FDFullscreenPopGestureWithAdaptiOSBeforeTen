//
//  secViewController.m
//  testNaviHidden
//
//  Created by RENREN on 2018/1/2.
//  Copyright © 2018年 RENREN. All rights reserved.
//

#import "secViewController.h"
#import "threeViewController.h"

@interface secViewController ()

@end

@implementation secViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)next:(id)sender {
    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:[threeViewController getStoryBoardName]];
    [self.navigationController pushViewController:vc animated:YES];
}

-(BOOL)getNaviHidden
{
    return YES;
}

@end
