//
//  BaseViewController.h
//  customVCHidden
//
//  Created by RENREN on 2018/1/3.
//  Copyright © 2018年 RENREN. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController

/**
 是否隐藏导航栏 default no （不隐藏）

 @return yes or no
 */
-(BOOL)getNaviHidden;

/**
 是否取消返回当前页面的返回手势 default no（不取消返回手势）

 @return yes or no
 */
-(BOOL)getPanBackAction;

/**
 在ios 10.0 以下使用的字段
 */
@property (nonatomic , assign) BOOL pushed;

/**
 导航栏隐藏 默认No
 */
@property (nonatomic , assign) BOOL rr_navHidden;

/**
 是否禁止当前页面的返回手势  默认No 不禁止
 */
@property (nonatomic , assign) BOOL rr_backActionDisAble;


@end
